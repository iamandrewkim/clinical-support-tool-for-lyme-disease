package edu.gatech;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.gatech.jil.idexx.counties.Counties;
import edu.gatech.jil.idexx.states.Datum;
import edu.gatech.jil.idexx.states.States;

import java.io.File;

public class Main {

    public static void dumpCountyData(String statePostalCode)
    {
        String id = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            States states = mapper.readValue(new File("./data/states/states.json"), States.class);

            for(Datum d : states.getData()) {
                if (d.getAbbrv().equalsIgnoreCase(statePostalCode)) {
                    id = d.getId().toString();
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println("Failed to read states.json");
        }

        if (id == null) return;

        try {
            Counties counties = mapper.readValue(new File("./data/counties/2018/" + id + ".json"), Counties.class);
            for (edu.gatech.jil.idexx.counties.Datum d : counties.getData()) {
                System.out.println("County name=" + d.getParams().getCounty().getName());
                System.out.println("  Positive: " + d.getStats().getPositive().toString() +
                        "  Tested: " + d.getStats().getTested().toString() +
                        "  Risk: " + d.getStats().getRisk());
            }

            int i = 0;
        } catch (Exception ex) {
            System.out.println("Failed to read county data for stateID=" + id);
        }
    }

    public static void main(String[] args) {
        System.out.println("Query IDEXX canine Lyme data.");
        System.out.println("  Specify state postal code as command-line argument for county data in 2018.");
        System.out.println("  No command-line argument dumps state data.");
        System.out.println();

        if (args.length > 0)
        {
            dumpCountyData(args[0]);
            return;
        }

        ObjectMapper mapper = new ObjectMapper();

        try {
            States states = mapper.readValue(new File("./data/states/states.json"), States.class);
            for(Datum d : states.getData()) {
                System.out.println("ID: " + d.getId() + "  FIPS: " + d.getFips() + "  State name: " + d.getName() + "  Postal code: " + d.getAbbrv());
            }
            System.out.println("Done.");
        } catch (Exception ex) {
            System.out.println("Failed to read states.json");
        }



    }
}
