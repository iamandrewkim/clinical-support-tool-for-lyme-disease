# Manual

Team Name: No Assignment Due This Week

Project Name: Just in Lyme

GitHub: [https://github.gatech.edu/gt-cs6440-hit-spring2019/Just-in-Lyme](https://github.gatech.edu/gt-cs6440-hit-spring2019/Just-in-Lyme)

Team Members:  Jason **Bennett**, Steven **Gray**, Andrew **Kim**, Dilek **Manzak**, Erick **Parra**, Matt **Stephenson**


## Login Page
When first visiting the application, you will be presented with a choice: either you must use the application as a patient or as a provider.  If you choose to use the application as a provider, a username and password would be required in a real setting but currently those inputs are disabled.

Patient Portal Login
![login page w/patient selected](images/login-patient.png)

Provider Portal Login
![login page w/provider selected](images/login-provider.png)


Entrance Criteria: Needs to determine recommendations for lyme disease for self or patient.

Exit Expectations: Attempt to find information on yourself or patients

Transitions: [Patient Finder](#patient_finder)

## <a name="patient_finder"></a>Patient Finder
Patients may be searched for via the FHIR service. The NADO family (No Assignment Due On) is prepopulated in the configured FHIR server (HapiFhir) and is the default search criteria. You can also search for patients by name. On HapiFhir, Jones is a popular family name.
![screen to search for a patient by ID or name](images/pick-patient.png)

Entrance Criteria: Unknown patient which needs to be identified

Exit Expectations: Patient found, or new patient.

Transitions: [Symptom Checking](#symptom_checking)

## <a name="symptom_checking"></a>Symptom Checking
When a patient has been found or created, the system will go through a selection of questions.  These quesitons may change depending on answers to previous questions. All questions which are presented must be answered.
![symptom checking screen example](images/symptom-checking.png)

Entrance Criteria: Patient has been found or created.

Exit Expectations: Symptom information has been collected.

Transitions: [Travel Survey](#travel_survey)

## <a name="travel_survey"></a>Travel Survey
A travel survey will help determine if the patient was in a high-risk area.  The data that is collected is done via county, and not all counties may be found. It is recommended to select the nearest county if the one which was visited is not in the list.

![travel survey screen example](images/travel.png)

Entrance Criteria: Patient has been found or created.

Exit Expectations: Symptom information has been collected.

Transitions: [Travel Survey](#travel_survey)

## <a name="recommendations"></a>Recommendations
Based on the given surveys, the CDC recommendations will be presented.

![recomendation screen example](images/recommendations.png)

Entrance Criteria: Travel Survey and Symptom Checking has completed.

Exit Expectations: N/A

Transitions: N/A
