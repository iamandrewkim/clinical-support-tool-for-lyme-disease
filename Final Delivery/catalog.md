# Table of Contents
Team Name: No Assignment Due This Week

Project Name: Just in Lyme

GitHub: [https://github.gatech.edu/gt-cs6440-hit-spring2019/Just-in-Lyme](https://github.gatech.edu/gt-cs6440-hit-spring2019/Just-in-Lyme)

Team Members:  Jason **Bennett**, Steven **Gray**, Andrew **Kim**, Dilek **Manzak**, Erick **Parra**, Matt **Stephenson**


Docker Link: [https://cs6440-s19-prj024.apps.hdap.gatech.edu/cs6440-s19-prj024/](https://cs6440-s19-prj024.apps.hdap.gatech.edu/cs6440-s19-prj024/)

Table Of Contents
=================

* catalog.pdf (this file)
* Manual - No Assignment Due This Week.pdf
* Special Instructions - No Assignment Due This Week.pdf
* Gantt_and_Status.pdf (whole semester and final weeks Gantts, and all weekly status reports)
* `Research/` - folder with various Lyme, Geographic, and Symptom specific docs and links
* build_and_run.bash (described in Special Instructions)
* README.md (makes GitHub directory view look nice)
* `../JustInLyme` (main Java app with pom.xml and Dockerfile)
* ../FHIR_PROJECT.postman_collection.json (creates the NADO family on HapiFhir)
* `../idexx` (scrapes IDEXX canine data into usable JSON files) 
* ../Jenkinsfile  (HDAP CI configuration)
* ../LOINC-and-CPT.txt (interesting LOINC and CPT codes used during test and dev)
* ../Spring2019CatalogProject54.pdf (original project spec)

