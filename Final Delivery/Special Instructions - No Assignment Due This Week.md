Special Instructions
====================

* Our app is successfully hosted on HDAP at https://cs6440-s19-prj024.apps.hdap.gatech.edu/cs6440-s19-prj024/login

* However, if you want to build and run locally, we have provided steps below.

* You need JDK8 and Maven 3 installed locally to build the application.

* Following HDAP containerization principle, you need Docker 18 installed locally to run the application.

* After those are installed, you can build, Dockerize, and start the app in one step:

* Run `build_and_run.bash`

* You will see maven do a WAR build, Docker make an image `justinlyme:00`, and Docker will start that image

* You will see Spring startup messages fly by, once the app says its is started, you can view it in browser:

* Navigate to http://localhost:8080/cs6440-s19-prj024/login

* Our user manual describes the full application flow which you can use for validation purposes.

* Please see the user manual in the `doc/` directory at the root of this repo.

* Caution: At least on Ubuntu 16.04, the Cisco GT VPN seems to get in the way of accessing the Docker-hosted site via localhost.
If the app is started, but the login screen will not load, try disconnecting from VPN, restarting the app, and restarting your
browser.

* Caution: With our TA Mentor's OK, we are using HAPIFHIR. Should HAPIFHIR be unavailable or should HAPIFHIR purge the "NADO" family
of patients (showing a blank screen on the patientPicker), please contact @mstephenson6 on the course Slack _immediately_ and we will speedily resolve 
this issue. The NADO family can be re-created via Postman JSON requests per our file `FHIR_PROJECT_postman_collection.json` at the repo root.
