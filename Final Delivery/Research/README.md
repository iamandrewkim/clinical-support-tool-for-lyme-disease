# Research
Team Name: No Assignment Due This Week
Project Name: Just in Lyme
GitHub: [https://github.gatech.edu/gt-cs6440-hit-spring2019/Just-in-Lyme](https://github.gatech.edu/gt-cs6440-hit-spring2019/Just-in-Lyme)
Team Members:  Jason **Bennett**, Steven **Gray**, Andrew **Kim**, Dilek **Manzak**, Erick **Parra**, Matt **Stephenson**

This folder represents the research sources that were used in design and development.
