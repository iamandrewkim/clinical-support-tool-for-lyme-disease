package edu.gatech.cs6440.JustInLyme;

import org.hl7.fhir.dstu3.model.Observation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

// White box tests
public class WhiteboxTests {

    public void WhiteboxTests() {}

    public void fhirService_getObservations() {
        FhirService fs = new FhirService();
        fs.setFhirServer("http://hapi.fhir.org/baseDstu3");
        List<Observation> obs = fs.getObservationByPatientID("cf-1545208956901");
        assert(obs.size() > 0);
    }
}
