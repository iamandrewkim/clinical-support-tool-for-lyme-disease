package edu.gatech.cs6440.JustInLyme;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BlackboxTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    /**
     * Fails with exception if build is sorely misconfigured
     */
    @Test
    public void testContextLoads() {
    }

    /**
     * Fails with assertion if something caused Boot to screw up its startup and rendering index page
     */
    @Test
    public void indexActuallyRenders() {
        ResponseEntity<String> body = testRestTemplate.getForEntity("/", String.class);
        assertTrue(body.getBody(), body.getBody().toLowerCase().contains("no assignment"));
    }

}
