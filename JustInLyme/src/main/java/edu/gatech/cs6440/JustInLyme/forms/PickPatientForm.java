package edu.gatech.cs6440.JustInLyme.forms;

public class PickPatientForm {
    private String pickedId;

    public String getPickedId() {
        return pickedId;
    }

    public void setPickedId(String pickedId) {
        this.pickedId = pickedId;
    }
}
