
package edu.gatech.cs6440.JustInLyme.idexx.counties;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "positive",
    "tested",
    "risk",
    "percentage",
    "ratio"
})
public class Stats {

    @JsonProperty("positive")
    private Integer positive;
    @JsonProperty("tested")
    private Integer tested;
    @JsonProperty("risk")
    private String risk;
    @JsonProperty("percentage")
    private Integer percentage;
    @JsonProperty("ratio")
    private Ratio ratio;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("positive")
    public Integer getPositive() {
        return positive;
    }

    @JsonProperty("positive")
    public void setPositive(Integer positive) {
        this.positive = positive;
    }

    @JsonProperty("tested")
    public Integer getTested() {
        return tested;
    }

    @JsonProperty("tested")
    public void setTested(Integer tested) {
        this.tested = tested;
    }

    @JsonProperty("risk")
    public String getRisk() {
        return risk;
    }

    @JsonProperty("risk")
    public void setRisk(String risk) {
        this.risk = risk;
    }

    @JsonProperty("percentage")
    public Integer getPercentage() {
        return percentage;
    }

    @JsonProperty("percentage")
    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    @JsonProperty("ratio")
    public Ratio getRatio() {
        return ratio;
    }

    @JsonProperty("ratio")
    public void setRatio(Ratio ratio) {
        this.ratio = ratio;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
