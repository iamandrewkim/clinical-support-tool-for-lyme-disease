package edu.gatech.cs6440.JustInLyme;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

import ca.uhn.fhir.model.api.BaseBundle;
import ca.uhn.fhir.rest.gclient.IQuery;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Bundle.BundleEntryComponent;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.ReferenceClientParam;
import org.hl7.fhir.instance.model.api.IBaseBundle;

@Service
public class FhirService {

    @Value("${app.fhirServer}")
    private String fhirServer;

//    USE IN TESTING ONLY!!!
    public void setFhirServer(String str) {
        fhirServer = str;
    }

    public List<Patient> getPatientsWithName(String searchForName) {
        IGenericClient client = FhirContext.forDstu3().newRestfulGenericClient(fhirServer);

        Bundle resultBundle = client.search()
      	      .forResource(Patient.class)
      	      .where(Patient.NAME.matches().values(searchForName))
      	      .returnBundle(Bundle.class)
      	      .execute();
        ArrayList<Patient> list = new ArrayList<>() ;
        for(int i=0;i<resultBundle.getEntry().size();i++) {
            list.add((Patient) resultBundle.getEntry().get(i).getResource());
        }
        return list;
    }

    public List<Observation> getObservationByPatientID(String patientID) {
        IGenericClient client = FhirContext.forDstu3().newRestfulGenericClient(fhirServer);

        Bundle bundle = client.search().forResource(Observation.class)
                .where(new ReferenceClientParam("patient").hasId(patientID))
                .returnBundle(Bundle.class)
                .execute();

        ArrayList<Observation> list = new ArrayList<>();
        for (BundleEntryComponent bund : bundle.getEntry()) {
            list.add((Observation)bund.getResource());
        }
        return list;
    }

}
