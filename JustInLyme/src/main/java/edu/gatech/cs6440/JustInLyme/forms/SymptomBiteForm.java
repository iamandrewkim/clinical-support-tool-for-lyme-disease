package edu.gatech.cs6440.JustInLyme.forms;

import edu.gatech.cs6440.JustInLyme.OrTheseSymptoms;

public class SymptomBiteForm implements OrTheseSymptoms {
    //q1: Severe headaches and neck stiffness
    private Boolean q1;
    public Boolean getQ1() {
        return q1;
    }
    public void setQ1(Boolean q1) {
        this.q1 = q1;
    }
    //q2: Erythema migrans (EM) rash
    private Boolean q2;
    public Boolean getQ2() {
        return q2;
    }
    public void setQ2(Boolean q2) {
        this.q2 = q2;
    }
    //q3: Arthritis with severe joint pain and swelling, particularly the knees and other large joints
    private Boolean q3;
    public Boolean getQ3(){
        return q3;
    }
    public void setQ3(Boolean q3){
        this.q3 = q3;
    }
    //q4: Facial palsy
    private Boolean q4;
    public Boolean getQ4(){
        return q4;
    }
    public void setQ4(Boolean q4){
        this.q4 = q4;
    }
    //q5: Intermittent pain in tendons, muscles, joints, and bones
    private Boolean q5;
    public Boolean getQ5(){
        return q5;
    }
    public void setQ5(Boolean q5){
        this.q5 = q5;
    }
    //q6: Heart palpitations or an irregular heart beat
    private Boolean q6;
    public Boolean getQ6(){
        return q6;
    }
    public void setQ6(Boolean q6){
        this.q6 = q6;
    }
    //q7: Dizziness or shortness of breath
    private Boolean q7;
    public Boolean getQ7(){
        return q7;
    }
    public void setQ7(Boolean q7){
        this.q7 = q7;
    }
    //q8: Inflammation of the brain and spinal cord
    private Boolean q8;
    public Boolean getQ8(){
        return q8;
    }
    public void setQ8(Boolean q8){
        this.q8 = q8;
    }
    //q9: Nerve pain
    private Boolean q9;
    public Boolean getQ9(){
        return q9;
    }
    public void setQ9(Boolean q9){
        this.q9 = q9;
    }
    //q10: Shooting pains, numbness, or tingling in the hands or feet
    private Boolean q10;
    public Boolean getQ10() {
        return q10;
    }
    public void setQ10(Boolean q10) {
        this.q10 = q10;
    }
    //q11: Short-term memory
    private Boolean q11;
    public Boolean getQ11() {
        return q11;
    }
    public void setQ11(Boolean q11) {
        this.q11 = q11;
    }

    public boolean booleanOrAllSymptoms() {
        return q1 || q2 || q3 || q4 || q5 || q6 || q7
                || q8 || q9 || q10 || q11;
    }
}
