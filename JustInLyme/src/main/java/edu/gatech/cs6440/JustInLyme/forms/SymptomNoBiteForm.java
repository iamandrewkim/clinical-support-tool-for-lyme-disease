package edu.gatech.cs6440.JustInLyme.forms;

import edu.gatech.cs6440.JustInLyme.OrTheseSymptoms;

public class SymptomNoBiteForm implements OrTheseSymptoms {
    //q1: Fever, chills, headache, fatigue, muscle and joint aches, and swollen lymph nodes
    private Boolean q1;
    public Boolean getQ1() {
        return q1;
    }
    public void setQ1(Boolean q1) {
        this.q1 = q1;
    }
    //q2: Erythema migrans (EM) rash
    private Boolean q2;
    public Boolean getQ2() {
        return q2;
    }
    public void setQ2(Boolean q2) {
        this.q2 = q2;
    }

    public boolean booleanOrAllSymptoms() {
        return q1 || q2;
    }
}