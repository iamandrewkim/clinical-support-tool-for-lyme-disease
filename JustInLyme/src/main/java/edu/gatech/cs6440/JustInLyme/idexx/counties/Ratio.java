
package edu.gatech.cs6440.JustInLyme.idexx.counties;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "is",
    "of"
})
public class Ratio {

    @JsonProperty("is")
    private Integer is;
    @JsonProperty("of")
    private Integer of;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("is")
    public Integer getIs() {
        return is;
    }

    @JsonProperty("is")
    public void setIs(Integer is) {
        this.is = is;
    }

    @JsonProperty("of")
    public Integer getOf() {
        return of;
    }

    @JsonProperty("of")
    public void setOf(Integer of) {
        this.of = of;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
