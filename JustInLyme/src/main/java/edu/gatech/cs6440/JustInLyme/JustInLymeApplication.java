package edu.gatech.cs6440.JustInLyme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JustInLymeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JustInLymeApplication.class, args);
	}

}
