package edu.gatech.cs6440.JustInLyme.forms;

public class Q1Form {
    private Boolean biteHappenedWithin30Days;

    public Boolean getBiteHappenedWithin30Days() {
        return biteHappenedWithin30Days;
    }

    public void setBiteHappenedWithin30Days(Boolean biteHappenedWithin30Days) {
        this.biteHappenedWithin30Days = biteHappenedWithin30Days;
    }
}
