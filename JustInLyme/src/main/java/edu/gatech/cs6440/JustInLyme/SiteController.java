package edu.gatech.cs6440.JustInLyme;

import edu.gatech.cs6440.JustInLyme.forms.*;
import edu.gatech.cs6440.JustInLyme.idexx.GeoService;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class SiteController {

    private final Logger logger = LoggerFactory.getLogger(SiteController.class);

    private FhirService fhirService;
    private GeoService geoService;
    private final List<String> viewNames = Collections.unmodifiableList(Arrays.asList(
            "login",
            "pickPatient",
            "symptomQ1",
            "symptomBite",
            "symptomNoBite",
            "travelSurvey",
            "providerRecommendations"
    ));

    private final String KEY_Q1 = "Q1";
    private final String KEY_RISK = "RISK";
    private final String KEY_SYMPTOMS = "SYMPTOMS";
    private final String KEY_PATIENT_SELECTED_ID = "PATIENT_ID";
    private final String KEY_PATIENTS_MAP = "PATIENTS_MAP";

    @Autowired
    public SiteController(FhirService fhirService,
                          GeoService geoService) {
        this.fhirService = fhirService;
        this.geoService = geoService;

    }

    @GetMapping("/")
    public ModelAndView index(HttpSession httpSession) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("viewNames", viewNames);
        if (httpSession.getAttribute(KEY_RISK) == null) {
            httpSession.setAttribute(KEY_RISK, "Unknown");
        }
        modelAndView.addObject("travelRisk", httpSession.getAttribute(KEY_RISK));
        return modelAndView;
    }

    @GetMapping("/login")
    public String login(HttpSession httpSession) {
        return "login";
    }

    @GetMapping("/providerRecommendations")
    public ModelAndView providerRecommendations(ProviderRecommendationsForm providerRecommendationsForm,
                                         HttpSession httpSession) {

        Boolean biteHappenedWithin30Days = (Boolean) httpSession.getAttribute(KEY_Q1);
        if (biteHappenedWithin30Days == null) {
            ModelAndView earlyExit = new ModelAndView();
            earlyExit.setViewName("redirect:/pickPatient");
            return earlyExit;
        }
        if (httpSession.getAttribute(KEY_RISK) == null) {
            httpSession.setAttribute(KEY_RISK, "Unknown");
        }
        String patientName, patientID;
        Optional<Patient> storedPatient = getPatientFromSession(httpSession);
        if (storedPatient.isPresent()) {
            Patient patient = storedPatient.get();
            patientName = patient.getName().get(0).getNameAsSingleString();
            patientID = patient.getId();
        }
        else {
            patientName = "NO SELECTION MADE";
            patientID = "NO SELECTION MADE";
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("providerRecommendations");
        providerRecommendationsForm.setPatientName(patientName);
        providerRecommendationsForm.setPatientId(patientID);
        String travelRisk = (String) httpSession.getAttribute(KEY_RISK);
        boolean highRisk = travelRisk.equalsIgnoreCase("High");
        providerRecommendationsForm.setHighRiskTravel(highRisk);

        Object obj_KeySymptoms = httpSession.getAttribute(KEY_SYMPTOMS);
        if (obj_KeySymptoms instanceof OrTheseSymptoms) {
            OrTheseSymptoms keySymptoms = (OrTheseSymptoms)obj_KeySymptoms;
            providerRecommendationsForm.setSymptomsToday(keySymptoms.booleanOrAllSymptoms());
        } else {
//            TODO: Report error to user?
            System.out.println("NO KEY_SYMPTOMS FOUND");
//            providerRecommendationsForm.setSymptomsToday(null);
        }
        if (storedPatient.isPresent()) {
            List<Observation> observations = fhirService.getObservationByPatientID(
                    storedPatient.get().getIdElement().getIdPart()
            );
            List<Observation> withValues = observations.stream()
                    .filter(Observation::hasValue)
                    .collect(Collectors.toList());
            providerRecommendationsForm.setPreviousObservations(withValues);
        }
        else {
            providerRecommendationsForm.setPreviousObservations(Collections.emptyList());
        }

        providerRecommendationsForm.setBiteHappenedMore30Days(biteHappenedWithin30Days);
        modelAndView.addObject("prForm", providerRecommendationsForm);
        return modelAndView;
    }

    @GetMapping("/travelSurvey")
    public ModelAndView travelSurvey(HttpSession httpSession) {
        String patientName, patientID;
        Optional<Patient> storedPatient = getPatientFromSession(httpSession);
        if (storedPatient.isPresent()) {
            Patient patient = storedPatient.get();
            patientName = patient.getName().get(0).getNameAsSingleString();
            patientID = patient.getId();
        }
        else {
            patientName = "NO SELECTION MADE";
            patientID = "NO SELECTION MADE";
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("states", geoService.getStates());
        modelAndView.addObject("patientName", patientName);
        modelAndView.addObject("patientID", patientID);
        modelAndView.setViewName("travelSurvey");
        return modelAndView;
    }

    @RequestMapping(value="/idexx/{state}", method= RequestMethod.GET)
    public ResponseEntity<List<String>> listAddCounties(@PathVariable("state") String state) {
        return new ResponseEntity<>(geoService.getCounties(state), HttpStatus.OK);
    }

    @RequestMapping(value="/idexx/{state}/{county}", method= RequestMethod.GET)
    public ResponseEntity<String> getCountyLymeValue(@PathVariable("state") String state,
                                                           @PathVariable("county") String county) {
        String val = geoService.riskOf(state, county);
        return new ResponseEntity<>("\"" + val + "\"", HttpStatus.OK);
    }

    @RequestMapping(value="/idexx/risk/{risk}", method=RequestMethod.GET)
    public ResponseEntity<String> newRisk(@PathVariable("risk") String risk, HttpSession httpSession) {
        String previousRisk = (String) httpSession.getAttribute(KEY_RISK);
        if (previousRisk == null) {
            previousRisk = "Unknown";
        }
        if (risk.equalsIgnoreCase("Unknown"))
            httpSession.setAttribute(KEY_RISK, risk);
        else if (risk.equalsIgnoreCase("None") && previousRisk.equalsIgnoreCase("Unknown"))
            httpSession.setAttribute(KEY_RISK, risk);
        else if (risk.equalsIgnoreCase("Low") && (previousRisk.equalsIgnoreCase("Unknown") || previousRisk.equalsIgnoreCase("None")))
            httpSession.setAttribute(KEY_RISK, risk);
        else if (risk.equalsIgnoreCase("Medium") && !previousRisk.equalsIgnoreCase("High"))
            httpSession.setAttribute(KEY_RISK, risk);
        else if (risk.equalsIgnoreCase("High"))
            httpSession.setAttribute(KEY_RISK, risk);
        return new ResponseEntity<>("\"" + "\"", HttpStatus.OK);
    }


    @GetMapping("/pickPatient")
    public ModelAndView pickPatient( @RequestParam(value = "search", required = false, defaultValue = "nado") String search, HttpSession httpSession, PickPatientForm pickPatientForm ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("pickPatient");
        modelAndView.addObject("pickPatientForm", pickPatientForm);
        String name = search;
        List<Patient> patients = fhirService.getPatientsWithName(name);
        modelAndView.addObject("patients", patients);
        Map<String,Patient> patientMap = patients.stream().collect(Collectors.toMap(p -> p.getId(), p -> p));
        httpSession.setAttribute(KEY_PATIENTS_MAP, patientMap);
        return modelAndView;
    }

    @PostMapping("/pickPatient/next")
    public String handlePickedPatient(HttpSession httpSession, PickPatientForm pickPatientForm) {
        logger.info("Picked " + pickPatientForm.getPickedId());
        httpSession.setAttribute(KEY_PATIENT_SELECTED_ID, pickPatientForm.getPickedId());
        return "redirect:/symptomQ1";
    }

    @GetMapping("/symptomQ1")
    public ModelAndView symptomQ1(Q1Form q1Form, HttpSession httpSession) {
        String patientName, patientID;
        Optional<Patient> storedPatient = getPatientFromSession(httpSession);
        if (storedPatient.isPresent()) {
            Patient patient = storedPatient.get();
            patientName = patient.getName().get(0).getNameAsSingleString();
            patientID = patient.getId();
        }
        else {
            patientName = "NO SELECTION MADE";
            patientID = "NO SELECTION MADE";
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("symptomQ1");
        modelAndView.addObject("q1Form", q1Form);
        modelAndView.addObject("patientName", patientName);
        modelAndView.addObject("patientId", patientID);
        return modelAndView;
    }

    @PostMapping("/symptomQ1/next")
    public String symptomQ1NextForm(Q1Form q1Form, HttpSession httpSession) {
        httpSession.setAttribute(KEY_Q1, q1Form.getBiteHappenedWithin30Days());
        if (q1Form.getBiteHappenedWithin30Days()) {
            return "redirect:/symptomBite";
        }
        return "redirect:/symptomNoBite";
    }

    @GetMapping("/symptomBite")
    public ModelAndView symptomBite(SymptomBiteForm biteForm, HttpSession httpSession) {
        String patientName, patientID;
        Optional<Patient> storedPatient = getPatientFromSession(httpSession);
        if (storedPatient.isPresent()) {
            Patient patient = storedPatient.get();
            patientName = patient.getName().get(0).getNameAsSingleString();
            patientID = patient.getId();
        }
        else {
            patientName = "NO SELECTION MADE";
            patientID = "NO SELECTION MADE";
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("symptomBite");
        modelAndView.addObject("biteForm", biteForm);
        modelAndView.addObject("patientName", patientName);
        modelAndView.addObject("patientId", patientID);
        return modelAndView;
    }

    @PostMapping("/symptomBite/next")
    public String symptomBiteNextForm(SymptomBiteForm biteForm, HttpSession httpSession) {
        httpSession.setAttribute(KEY_SYMPTOMS, biteForm);
        if (biteForm.booleanOrAllSymptoms()) {
            return "redirect:/travelSurvey";
        }
        return "redirect:/providerRecommendations";
    }

    @GetMapping("/symptomNoBite")
    public ModelAndView symptomNoBite(SymptomNoBiteForm noBiteForm, HttpSession httpSession) {
        String patientName, patientID;
        Optional<Patient> storedPatient = getPatientFromSession(httpSession);
        if (storedPatient.isPresent()) {
            Patient patient = storedPatient.get();
            patientName = patient.getName().get(0).getNameAsSingleString();
            patientID = patient.getId();
        }
        else {
            patientName = "NO SELECTION MADE";
            patientID = "NO SELECTION MADE";
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("symptomNoBite");
        modelAndView.addObject("noBiteForm", noBiteForm);
        modelAndView.addObject("patientName", patientName);
        modelAndView.addObject("patientId", patientID);
        return modelAndView;
    }

    @PostMapping("/symptomNoBite/next")
    public String symptomNoBiteNextForm(SymptomNoBiteForm noBiteForm, HttpSession httpSession) {
        httpSession.setAttribute(KEY_SYMPTOMS, noBiteForm);
        if (noBiteForm.booleanOrAllSymptoms()) {
            return "redirect:/travelSurvey";
        }
        return "redirect:/providerRecommendations";
    }

    private Optional<Patient> getPatientFromSession(HttpSession session) {
        String patientId = (String) session.getAttribute(KEY_PATIENT_SELECTED_ID);
        if (patientId == null) {
            return Optional.empty();
        }
        Map<String,Patient> map = (Map<String,Patient>) session.getAttribute(KEY_PATIENTS_MAP);
        Patient patient = map.get(patientId);
        return Optional.ofNullable(patient);
    }
}
