package edu.gatech.cs6440.JustInLyme.idexx;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.gatech.cs6440.JustInLyme.idexx.counties.Counties;
import edu.gatech.cs6440.JustInLyme.idexx.states.Datum;
import edu.gatech.cs6440.JustInLyme.idexx.states.States;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class GeoService {
    public static String[] States = {"Georgia", "Oregon"};

    public List<String> getStates() {
        List<String> stateList = new ArrayList<>();

        ObjectMapper mapper = new ObjectMapper();
        try {
            States states = mapper.readValue(new ClassPathResource("data/states/states.json").getInputStream(), States.class);
            for(Datum d : states.getData()) {
                stateList.add(d.getName());
            }
        } catch (Exception ex) {
        //    System.out.println("Failed to read states.json");
            System.out.println(ex.toString());
        }

        return stateList;
    }

    public List<String> getCounties(String state) {
        List<String> countyList = new ArrayList<>();

        String id = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            States states = mapper.readValue(new ClassPathResource("data/states/states.json").getInputStream(), States.class);

            for(Datum d : states.getData()) {
                if (d.getName().equalsIgnoreCase(state)) {
                    id = d.getId().toString();
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println("Failed to read states.json");
        }

        if (id == null) return countyList;

        try {
            Counties counties = mapper.readValue(new ClassPathResource("data/counties/2018/" + id + ".json").getInputStream(), Counties.class);
            for (edu.gatech.cs6440.JustInLyme.idexx.counties.Datum d : counties.getData()) {
                countyList.add(d.getParams().getCounty().getName());
//                System.out.println("County name=" + d.getParams().getCounty().getName());
//                System.out.println("  Positive: " + d.getStats().getPositive().toString() +
//                        "  Tested: " + d.getStats().getTested().toString() +
//                        "  Risk: " + d.getStats().getRisk());
            }

            int i = 0;
        } catch (Exception ex) {
            System.out.println("Failed to read county data for stateID=" + id);
        }

        return countyList;
    }

    public String riskOf(String state, String county) {
//        int index = Math.abs (state.hashCode() + county.hashCode()) % 3;
//        String[] risks = new String[] {"Low", "Medium", "High"};
//        return risks[index];
        String id = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            States states = mapper.readValue(new ClassPathResource("data/states/states.json").getInputStream(), States.class);

            for(Datum d : states.getData()) {
                if (d.getName().equalsIgnoreCase(state)) {
                    id = d.getId().toString();
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println("Failed to read states.json");
        }

        if (id == null) return "Unknown";

        try {
            Counties counties = mapper.readValue(new ClassPathResource("data/counties/2018/" + id + ".json").getInputStream(), Counties.class);
            for (edu.gatech.cs6440.JustInLyme.idexx.counties.Datum d : counties.getData()) {
                if (d.getParams().getCounty().getName().equalsIgnoreCase(county))
                    return d.getStats().getRisk();
            }

            int i = 0;
        } catch (Exception ex) {
            System.out.println("Failed to read county data for stateID=" + id);
        }

        return "Unknown";
    }

}
