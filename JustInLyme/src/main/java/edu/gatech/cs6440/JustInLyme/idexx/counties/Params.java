
package edu.gatech.cs6440.JustInLyme.idexx.counties;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "county",
    "month",
    "year",
    "species",
    "disease"
})
public class Params {

    @JsonProperty("county")
    private County county;
    @JsonProperty("month")
    private Object month;
    @JsonProperty("year")
    private String year;
    @JsonProperty("species")
    private Integer species;
    @JsonProperty("disease")
    private Integer disease;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("county")
    public County getCounty() {
        return county;
    }

    @JsonProperty("county")
    public void setCounty(County county) {
        this.county = county;
    }

    @JsonProperty("month")
    public Object getMonth() {
        return month;
    }

    @JsonProperty("month")
    public void setMonth(Object month) {
        this.month = month;
    }

    @JsonProperty("year")
    public String getYear() {
        return year;
    }

    @JsonProperty("year")
    public void setYear(String year) {
        this.year = year;
    }

    @JsonProperty("species")
    public Integer getSpecies() {
        return species;
    }

    @JsonProperty("species")
    public void setSpecies(Integer species) {
        this.species = species;
    }

    @JsonProperty("disease")
    public Integer getDisease() {
        return disease;
    }

    @JsonProperty("disease")
    public void setDisease(Integer disease) {
        this.disease = disease;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
