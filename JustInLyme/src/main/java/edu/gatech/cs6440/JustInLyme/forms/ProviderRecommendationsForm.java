package edu.gatech.cs6440.JustInLyme.forms;

import org.hl7.fhir.dstu3.model.Observation;

import java.util.List;

public class ProviderRecommendationsForm {
    private String patientName;
    private String patientId;
    private Boolean highRiskTravel;
    private List<Observation> previousObservations;
    private Boolean symptomsToday;
    private Boolean biteHappenedMore30Days;

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public Boolean getHighRiskTravel() {
        return highRiskTravel;
    }

    public void setHighRiskTravel(Boolean highRiskTravel) {
        this.highRiskTravel = highRiskTravel;
    }

    public Boolean getSymptomsToday() {
        return symptomsToday;
    }

    public void setSymptomsToday(Boolean symptomsToday) {
        this.symptomsToday = symptomsToday;
    }

    public List<Observation> getPreviousObservations() {
        return previousObservations;
    }

    public void setPreviousObservations(List<Observation> previousObservations) {
        this.previousObservations = previousObservations;
    }

    public Boolean getBiteHappenedMore30Days() {
        return biteHappenedMore30Days;
    }

    public void setBiteHappenedMore30Days(Boolean biteHappenedMore30Days) {
        this.biteHappenedMore30Days = biteHappenedMore30Days;
    }
}
